/* Copyright Applied Industrial Logic Limited 2018. All rights Reserved */
package com.ail.insurance.quotation;

import com.ail.annotation.ServiceArgument;
import com.ail.annotation.ServiceCommand;
import com.ail.annotation.ServiceInterface;
import com.ail.core.command.Argument;
import com.ail.core.command.Command;
import com.ail.insurance.policy.Policy;

@ServiceInterface
public interface InitialiseCancellationService {

    @ServiceArgument
    public interface InitialiseCancellationArgument extends Argument {
        void setPolicyArg(Policy policyArg);

        Policy getPolicyArg();

        void setCancellationQuotationArg(Policy cancellationQuotationArg);

        Policy getCancellationQuotationArg();
    }

    @ServiceCommand
    public interface InitialiseCancellationCommand extends Command, InitialiseCancellationArgument {
    }
}
