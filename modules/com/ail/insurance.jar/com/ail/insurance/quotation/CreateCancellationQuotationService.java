/* Copyright Applied Industrial Logic Limited 2018. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

package com.ail.insurance.quotation;

import static com.ail.core.CoreContext.getCoreProxy;
import static com.ail.insurance.policy.PolicyLinkType.CANCELLATION_FOR;
import static com.ail.insurance.policy.PolicyLinkType.CANCELLATION_QUOTATION_FOR;
import static com.ail.insurance.policy.PolicyLinkType.CANCELLATION_QUOTATION_FROM;
import static com.ail.insurance.policy.PolicyLinkType.MTA_QUOTATION_FOR;
import static com.ail.insurance.policy.PolicyLinkType.RENEWAL_QUOTATION_FOR;
import static com.ail.insurance.policy.PolicyStatus.APPLICATION;
import static com.ail.insurance.policy.PolicyStatus.APPLIED;
import static com.ail.insurance.policy.PolicyStatus.NOT_TAKEN_UP;
import static com.ail.insurance.policy.PolicyStatus.ON_RISK;
import static com.ail.insurance.policy.PolicyStatus.SUBMITTED;
import static java.lang.String.format;
import static java.util.Arrays.asList;

import java.util.List;

import com.ail.annotation.ServiceArgument;
import com.ail.annotation.ServiceCommand;
import com.ail.annotation.ServiceImplementation;
import com.ail.core.BaseException;
import com.ail.core.PostconditionException;
import com.ail.core.PreconditionError;
import com.ail.core.PreconditionException;
import com.ail.core.Service;
import com.ail.core.command.Argument;
import com.ail.core.command.Command;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.policy.PolicyLink;
import com.ail.insurance.policy.PolicyLinkType;
import com.ail.insurance.quotation.CreateCancellationQuotationService.CreateCancellationQuotationArgument;
import com.ail.insurance.quotation.InitialiseCancellationService.InitialiseCancellationCommand;
import com.ail.insurance.quotation.PrepareRequoteService.PrepareRequoteCommand;

/**
 */
@ServiceImplementation
public class CreateCancellationQuotationService extends Service<CreateCancellationQuotationArgument> {
    private static final long serialVersionUID = 3819563603833694389L;
    private static List<PolicyLinkType> POLICY_LINKS_TO_NTU = asList(MTA_QUOTATION_FOR, CANCELLATION_FOR, RENEWAL_QUOTATION_FOR);

    @ServiceArgument
    public interface CreateCancellationQuotationArgument extends Argument {
        void setPolicyArg(Policy policy);

        Policy getPolicyArg();

        void setCancellationRet(Policy cancellationRet);

        Policy getCancellationRet();
    }

    @ServiceCommand(defaultServiceClass = CreateCancellationQuotationService.class)
    public interface CreateCancellationQuotationCommand extends Command, CreateCancellationQuotationArgument {
    }

    @Override
    public void invoke() throws BaseException {
        if (args.getPolicyArg() == null) {
            throw new PreconditionException("args.getPolicyArg() == null");
        }

        if (args.getPolicyArg().getStatus() != ON_RISK) {
            throw new PreconditionException("args.getPolicyArg().getStatus() != ON_RISK");
        }

        markExistingQuotationsAsNotTakenUp();

        createCancellationQuotation();

        getCoreProxy().flush(); // flush the Cancellation so we get IDs etc. populated.

        linkQuotationToPolicy();

        setCancellationStatusToApplication();

        initialiseCancellationQuotation();

        if (args.getCancellationRet() == null) {
            throw new PostconditionException("args.getCancellationRet() == null");
        }
    }

    private void setCancellationStatusToApplication() {
        args.getCancellationRet().setStatus(APPLICATION);
    }

    private void initialiseCancellationQuotation() throws BaseException {
        InitialiseCancellationCommand icc = getCoreProxy().newCommand("InitialiseCancellation", InitialiseCancellationCommand.class);
        icc.setPolicyArg(args.getPolicyArg());
        icc.setCancellationQuotationArg(args.getCancellationRet());
        icc.invoke();
    }

    private void linkQuotationToPolicy() {
        args.getPolicyArg().getPolicyLink().add(new PolicyLink(CANCELLATION_QUOTATION_FOR, args.getCancellationRet().getSystemId()));
        args.getCancellationRet().getPolicyLink().add(new PolicyLink(CANCELLATION_QUOTATION_FROM, args.getPolicyArg().getSystemId()));
    }

    private void createCancellationQuotation() throws BaseException {
        PrepareRequoteCommand prc = getCoreProxy().newCommand(PrepareRequoteCommand.class);
        prc.setPolicyArg(args.getPolicyArg());
        prc.setSuppressDocumentCopyArg(true);
        prc.invoke();

        args.setCancellationRet(getCoreProxy().create(prc.getRequoteRet()));
    }

    /**
     * Search for all quotations that were created for this policy in the past.
     * Any that are found will which have not been applied are marked as NOT_TAKEN_UP.
     * @throws PreconditionException if any SUBMITTED quotations are linked to.
     */
    private void markExistingQuotationsAsNotTakenUp() throws PreconditionException {
        try {
            args.getPolicyArg().getPolicyLink().
                    stream().
                    filter(pl -> POLICY_LINKS_TO_NTU.contains(pl.getLinkType())).
                    forEach(pl -> {
                        Policy oldQuotation = (Policy)getCoreProxy().queryUnique("get.policy.by.systemId", pl.getTargetPolicyId());
                        if (oldQuotation.getStatus() != APPLIED && oldQuotation.getStatus() != SUBMITTED) {
                            oldQuotation.setStatus(NOT_TAKEN_UP);
                        }
                        else if (oldQuotation.getStatus() == SUBMITTED) {
                            throw new PreconditionError(format("Cancellation blocked by submitted quotation for: %s, but policy: %s", pl.getLinkType(), pl.getTargetPolicyId()));
                        }
                    });
        }
        catch(PreconditionError error) {
            throw new PreconditionException(error.getDescription());
        }
    }
}
