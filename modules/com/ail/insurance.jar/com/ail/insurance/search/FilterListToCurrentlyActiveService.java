/* Copyright Applied Industrial Logic Limited 2018. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

package com.ail.insurance.search;

import static com.ail.core.Functions.isEmpty;
import static com.ail.insurance.policy.PolicyStatus.DELETED;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.ail.annotation.ServiceArgument;
import com.ail.annotation.ServiceCommand;
import com.ail.annotation.ServiceImplementation;
import com.ail.core.BaseException;
import com.ail.core.PreconditionException;
import com.ail.core.Service;
import com.ail.core.command.Argument;
import com.ail.core.command.Command;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.search.FilterListToCurrentlyActiveService.FilterListToCurrentlyActiveArgument;

@ServiceImplementation
public class FilterListToCurrentlyActiveService extends Service<FilterListToCurrentlyActiveArgument> {

    @ServiceArgument
    public interface FilterListToCurrentlyActiveArgument extends Argument {

        List<Policy> getPoliciesArg();

        void setPoliciesArg(List<Policy> policiesArg);

        List<Policy> getFilteredPoliciesRet();

        void setFilteredPoliciesRet(List<Policy> filteredPoliciesRet);
    }

    @ServiceCommand(defaultServiceClass = FilterListToCurrentlyActiveService.class)
    public interface FilterListToCurrentlyActiveCommand extends Command, FilterListToCurrentlyActiveArgument {
    }

    @Override
    public void invoke() throws BaseException {
        if (args.getPoliciesArg() == null) {
            throw new PreconditionException("args.getPoliciesArgRet() == null");
        }

        args.setFilteredPoliciesRet(filterActivePolicies(args.getPoliciesArg()));
    }

    private List<Policy> filterActivePolicies(List<Policy> policies) {
        Map<String, Policy> filtered = new LinkedHashMap<>();

        for (Policy policy : policies) {
            if (policy.getStatus() != DELETED) {
                if (!filtered.containsKey(filterIdFor(policy)) || policy.isNewerByIndexThan(filtered.get(filterIdFor(policy)))) {
                    filtered.put(filterIdFor(policy), policy);
                }
            }
        }

        return new ArrayList<>(filtered.values());
    }

    private String filterIdFor(Policy policy) {
        return isEmpty(policy.getPolicyNumber()) ? Long.toString(policy.getSystemId()) : policy.getPolicyNumber();
    }
}
