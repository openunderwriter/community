/* Copyright Applied Industrial Logic Limited 2018. All rights reserved. */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.ail.core;

import java.util.List;

/**
 * Interface describing model objects that have references to exceptions. Any given
 * object may have 0..* exception records which describe exceptions that have been
 * thrown during the processing of the owning entity.
 */
public interface HasExceptions {
    List<ExceptionRecord> getException();

    void setException(List<ExceptionRecord> exception);

    void addException(ExceptionRecord exception);
}
