package com.ail.core.logging;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.hibernate.annotations.Index;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.ail.annotation.TypeDefinition;
import com.ail.core.Type;

/**
 * Records the details of HTTP request/responses processed by the RestfulBridge.
 */
@Entity
@TypeDefinition
@NamedQueries({
    @NamedQuery(name = "get.record.by.policy.id", query = "select rec from ServiceRequestRecord rec where rec.externalPolicyId = ?"),
})
public class ServiceRequestRecord extends Type {

    private static final int REQUEST_MAX_LENGHT = 4096;
    private static final int RESPONSE_MAX_LENGHT = 4096;

    @Column(columnDefinition="DATETIME")
    private Timestamp exitTimestamp;

    @Column(columnDefinition="DATETIME")
    private Timestamp entryTimestamp;

    private String product;

    private String command;

    @Index(name = "externalPolicyId")
    private String externalPolicyId;

    @Column(length = REQUEST_MAX_LENGHT)
    private String request;

    @Column(length = RESPONSE_MAX_LENGHT)
    private String response;

    Long userId;

    public ServiceRequestRecord() {
    }

    public ServiceRequestRecord(String product, String command, String op, String externalPolicyId, Long userId) {
        super();
        this.entryTimestamp = new Timestamp(System.currentTimeMillis());
        this.product = product;
        this.externalPolicyId = externalPolicyId;
        this.userId = userId;
        this.command = command;
        appendToCommand(op);
    }

    public ServiceRequestRecord(String product, String pageFlowName, String pageName, String op, String externalPolicyId, Long userId) {
        this(product, pageFlowName + (pageName != null ? "/" + pageName : ""), op, externalPolicyId, userId);
    }

    public Timestamp getExitTimestamp() {
        return exitTimestamp;
    }

    public void setExitTimestamp(Timestamp date) {
        this.exitTimestamp = date;
    }

    public void setExitTimestamp() {
        setExitTimestamp(new Timestamp(System.currentTimeMillis()));
    }

    public Timestamp getEntryTimestamp() {
        return entryTimestamp;
    }

    public void setEntryTimestamp(Timestamp date) {
        this.entryTimestamp = date;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        if (request!=null && request.length() >= REQUEST_MAX_LENGHT) {
            this.request = request.substring(0, REQUEST_MAX_LENGHT);
        } else {
            this.request = request;
        }
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        if (response!=null && response.length() >= RESPONSE_MAX_LENGHT) {
            this.response = response.substring(0, RESPONSE_MAX_LENGHT);
        } else {
            this.response = response;
        }
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public void appendToCommand(String op) {
        this.command = command + (op != null ? "/" + op : "");
    }

    public String getExternalPolicyId() {
        return externalPolicyId;
    }

    public void setExternalPolicyId(String externalPolicyId) {
        this.externalPolicyId = externalPolicyId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.typeHashCode();
        result = prime * result + ((command == null) ? 0 : command.hashCode());
        result = prime * result + ((entryTimestamp == null) ? 0 : entryTimestamp.hashCode());
        result = prime * result + ((exitTimestamp == null) ? 0 : exitTimestamp.hashCode());
        result = prime * result + ((externalPolicyId == null) ? 0 : externalPolicyId.hashCode());
        result = prime * result + ((product == null) ? 0 : product.hashCode());
        result = prime * result + ((request == null) ? 0 : request.hashCode());
        result = prime * result + ((response == null) ? 0 : response.hashCode());
        result = prime * result + ((userId == null) ? 0 : (int) (userId ^ (userId >>> 32)));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ServiceRequestRecord other = (ServiceRequestRecord) obj;
        if (command == null) {
            if (other.command != null)
                return false;
        } else if (!command.equals(other.command))
            return false;
        if (entryTimestamp == null) {
            if (other.entryTimestamp != null)
                return false;
        } else if (!entryTimestamp.equals(other.entryTimestamp))
            return false;
        if (exitTimestamp == null) {
            if (other.exitTimestamp != null)
                return false;
        } else if (!exitTimestamp.equals(other.exitTimestamp))
            return false;
        if (externalPolicyId == null) {
            if (other.externalPolicyId != null)
                return false;
        } else if (!externalPolicyId.equals(other.externalPolicyId))
            return false;
        if (product == null) {
            if (other.product != null)
                return false;
        } else if (!product.equals(other.product))
            return false;
        if (request == null) {
            if (other.request != null)
                return false;
        } else if (!request.equals(other.request))
            return false;
        if (response == null) {
            if (other.response != null)
                return false;
        } else if (!response.equals(other.response))
            return false;
        if (userId == null) {
            if (other.userId != null)
                return false;
        } else if (!userId.equals(other.userId))
            return false;
        return super.typeEquals(other);
    }
}
