/* Copyright Applied Industrial Logic Limited 2017. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

package com.ail.financial.ledger;

import static java.math.BigDecimal.ZERO;
import static javax.persistence.CascadeType.DETACH;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import static javax.persistence.EnumType.STRING;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.envers.Audited;

import com.ail.annotation.TypeDefinition;
import com.ail.core.Reference;
import com.ail.core.Type;
import com.ail.financial.CurrencyAmount;
import com.ail.financial.MoneyProvision;

@Audited
@Entity
@NamedQueries({
    @NamedQuery(name = "get.journal.by.origin.and.type.and.transaction.date.range",
                query = "select jou from Journal jou where jou.origin = ? and type = ? and jou.transactionDate >= ? and jou.transactionDate <= ?"),
    @NamedQuery(name = "get.journal.by.subject",
                query = "select jou from Journal jou where jou.subject.type = ? and jou.subject.refId = ?"),
    @NamedQuery(name = "get.journals.by.subject.and.type.orderby.transaction.date.descending",
                query = "select jou from Journal jou where jou.subject.type = ? and jou.subject.refId = ? and jou.type in (:journalTypes) order by jou.transactionDate desc"),
    @NamedQuery(name = "get.journals.by.subject.and.not.type.orderby.transaction.date.descending",
                query = "select jou from Journal jou where jou.subject.type = ? and jou.subject.refId = ? and jou.type not in (:journalTypes) order by jou.transactionDate desc"),
    @NamedQuery(name = "get.journal.by.subject.and.journal.type",
                query = "select jou from Journal jou where jou.subject.type = ? and jou.subject.refId = ? and jou.type = ?")
})
public class Journal extends Type {

    private Date transactionDate;
    private Date postedDate;
    private Date writtenDate;

    @OneToMany(cascade = {REFRESH, DETACH, PERSIST})
    private List<JournalLine> journalLine;

    @OneToOne(cascade = {REFRESH, DETACH, PERSIST})
    @JoinColumn(name = "contraOfUIDjou", referencedColumnName = "UID")
    private Journal contraOf;

    private String contraReason;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "type", column = @Column(name = "subjectType")),  @AttributeOverride(name = "refId", column = @Column(name = "subjectId"))
    })
    private Reference subject;

    @Enumerated(STRING)
    private JournalType type;

    @OneToOne(cascade = {REFRESH, DETACH, PERSIST})
    @JoinColumn(name = "originUIDmpr", referencedColumnName = "UID")
    private MoneyProvision origin;

    @OneToOne(cascade = {REFRESH, DETACH, PERSIST})
    @JoinColumn(name = "accountingPeriodUIDape", referencedColumnName = "UID")
    AccountingPeriod accountingPeriod;

    private String reason;

    @Column(columnDefinition = "BIT", name="jouHasBeenContrad")
    private boolean hasBeenContrad;

    @ManyToMany(mappedBy = "journal")
    private Set<JournalGroup> journalGroup;

    /** Only for framework use.
     * @deprecated Use {@link JournalBuilder} to create instances of Journal.
     */
    @Deprecated
    public Journal() {
    }

    private Journal(JournalBuilder builder) throws LedgerValidationException {
        transactionDate = builder.transactionDate;
        writtenDate = builder.writtenDate;
        journalLine = builder.journalLine;
        contraOf = builder.contraOf;
        subject = builder.subject;
        type = builder.type;
        postedDate = new Date();
        origin = builder.origin;
        reason = builder.reason;
        contraReason = builder.contraReason;
        journalGroup = builder.journalGroup;

        for(JournalLine line: journalLine) {
            line.setJournal(this);
        }

        for(JournalGroup group: journalGroup) {
            group.add(this);
        }
    }

    public Journal getContraOf() {
        return contraOf;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public Date getWrittenDate() {
        return writtenDate;
    }

    /**
     * Return an unmodifiable list of the journal lines associated with this Journal.
     * @return List<JournalLine>
     */
    public List<JournalLine> getJournalLine() {
        return Collections.unmodifiableList(journalLine);
    }

    public Date getPostedDate() {
        return postedDate;
    }

    public Reference getSubject() {
        return subject;
    }

    public JournalType getType() {
        return type;
    }

    public MoneyProvision getOrigin() {
        return origin;
    }

    public String getReason() {
        return reason;
    }

    public String getContraReason() {
        return contraReason;
    }

    public AccountingPeriod getAccountingPeriod() {
        return accountingPeriod;
    }

    /**
     * True if the Journal is a contra of another Journal.
     * @return true if this is a contra, false otherwise.
     */
    public boolean isContra() {
        return contraOf != null;
    }

    /**
     * True if this Journal has a contra associated with it - i.e. if this Journal has been contra'ed.
     * @return true if this has been contra'ed, false otherwise.
     */
    public boolean getHasBeenContrad() {
        return hasBeenContrad;
    }

    public void setHasBeenContrad(boolean hasBeenContrad) {
        this.hasBeenContrad = hasBeenContrad;
    }

    public Set<JournalGroup> getJournalGroup() {
        if (journalGroup == null) {
            journalGroup = new HashSet<>();
        }

        return journalGroup;
    }

    public void setJournalGroup(Set<JournalGroup> journalGroup) {
        this.journalGroup = journalGroup;
    }

    public void initialiseAccountingPeriod(AccountingPeriod accountingPeriod) {
        if (this.accountingPeriod == null) {
            this.accountingPeriod = accountingPeriod;
        }
    }

    @TypeDefinition
    public static class JournalBuilder {
        private Date transactionDate;
        private Date writtenDate;
        private List<JournalLine> journalLine = new ArrayList<>();
        private Journal contraOf;
        private JournalType type;
        private Reference subject;
        private MoneyProvision origin;
        private String reason;
        private String contraReason;
        private Set<JournalGroup> journalGroup = new HashSet<>();

        public JournalBuilder withTransactionDate(Date date) {
            this.transactionDate = date;
            return this;
        }

        public JournalBuilder withWrittenDate(Date date) {
            this.writtenDate = date;
            return this;
        }

        public JournalBuilder with(JournalLine... journalLine) {
            this.journalLine.addAll(Arrays.asList(journalLine));
            return this;
        }

        public JournalBuilder with(Set<JournalLine> journalLine) {
            this.journalLine.addAll(journalLine);
            return this;
        }

        public Journal build() throws LedgerValidationException {
            validate();
            return new Journal(this);
        }

        public JournalBuilder ofType(JournalType type) {
            this.type = type;
            return this;
        }

        public JournalBuilder contraOf(Journal contraOf) {
            this.contraOf = contraOf;
            return this;
        }

        public JournalBuilder subject(Reference subject) {
            this.subject= subject;
            return this;
        }

        public JournalBuilder forOrigin(MoneyProvision origin) {
            this.origin = origin;
            return this;
        }

        public JournalBuilder withReason(String reason) {
            this.reason = reason;
            return this;
        }

        public JournalBuilder withContraReason(String contraReason) {
            this.contraReason = contraReason;
            return this;
        }

        public JournalBuilder inGroup(JournalGroup journalGroup) {
            this.journalGroup.add(journalGroup);
            return this;
        }

        private void validate() throws LedgerValidationException {
            if (transactionDate == null) {
                throw new LedgerValidationException("date == null");
            }

            if (journalLine.size() < 2) {
                throw new LedgerValidationException("journalLine.size() < 2");
            }

            if (type == null) {
                throw new LedgerValidationException("type == null");
            }

            if (jouralLinesDontBalance(journalLine)) {
                throw new LedgerValidationException("journalDoesNotBalance() == true");
            }

            if (journalLinesPredateAccountOpening(journalLine)) {
                throw new LedgerValidationException("journalLinesPredateAccountOpening() == true");
            }

            for(JournalLine line: journalLine) {
                if (line.getJournal() != null) {
                    throw new LedgerValidationException("line.getJournal() != null");
                }
            }
        }

        private boolean jouralLinesDontBalance(List<JournalLine> lines) {
            CurrencyAmount total = null;

            for(JournalLine line: lines) {
                if (total == null) {
                    total = new CurrencyAmount(0, line.getAmount().getCurrency());
                }

                switch(line.getTransactionType()) {
                case CREDIT: total = total.add(line.getAmount()); break;
                case DEBIT: total = total.subtract(line.getAmount()); break;
                }
            }

            return ZERO.compareTo(total.getAmount()) != 0;
        }

        private boolean journalLinesPredateAccountOpening(List<JournalLine> lines) {
            for(JournalLine line: lines) {
                if (!transactionDate.after(line.getLedger().getAccount().getOpeningDate())) {
                    return true;
                }
            }

            return false;
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.typeHashCode();
        result = prime * result + ((contraOf == null) ? 0 : contraOf.hashCode());
        result = prime * result + ((transactionDate == null) ? 0 : transactionDate.hashCode());
        result = prime * result + ((journalLine == null) ? 0 : journalLine.hashCode());
        result = prime * result + ((reason == null) ? 0 : reason.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Journal other = (Journal) obj;
        if (contraOf == null) {
            if (other.contraOf != null)
                return false;
        } else if (!contraOf.equals(other.contraOf))
            return false;
        if (transactionDate == null) {
            if (other.transactionDate != null)
                return false;
        } else if (!transactionDate.equals(other.transactionDate))
            return false;
        if (journalLine == null) {
            if (other.journalLine != null)
                return false;
        } else if (!journalLine.equals(other.journalLine))
            return false;
        if (reason == null) {
            if (other.reason != null)
                return false;
        } else if (!reason.equals(other.reason))
            return false;
        if (contraReason == null) {
            if (other.contraReason != null)
                return false;
        } else if (!contraReason.equals(other.contraReason))
            return false;
        if (hasBeenContrad != other.hasBeenContrad)
                return false;
        return super.typeEquals(obj);
    }

    @Override
    public String toString() {
        return "Journal [date=" + transactionDate + ", journalLine=" + journalLine + ", contraOf=" + contraOf + ", getSystemId()=" + getSystemId() + ", getCreatedDate()=" + getCreatedDate()
                + ", getUpdatedDate()=" + getUpdatedDate() + ", getCreatedBy()=" + getCreatedBy() + ", getUpdatedBy()=" + getUpdatedBy() + ", getExternalSystemId()=" + getExternalSystemId() + ", getReason()=" + getReason() + ", getContraReason()=" + getContraReason()  + ", getHasBeenContraed()=" + getHasBeenContrad() + "]";
    }
}
