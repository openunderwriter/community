/* Copyright Applied Industrial Logic Limited 2018. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

package com.ail.pageflow.service;

import static com.ail.core.CoreContext.getCoreProxy;
import static com.ail.pageflow.PageFlowContext.getCurrentPageName;
import static com.ail.pageflow.PageFlowContext.getPageFlowName;

import java.util.Date;

import com.ail.annotation.ServiceImplementation;
import com.ail.core.BaseException;
import com.ail.core.CoreContext;
import com.ail.core.PageVisit;
import com.ail.core.PreconditionException;
import com.ail.core.Service;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.quotation.CreateRenewalQuotationService.CreateRenewalQuotationCommand;
import com.ail.pageflow.ExecutePageActionService;
import com.ail.pageflow.PageFlowContext;

@ServiceImplementation
public class CreateRenewalQuotationService extends Service<ExecutePageActionService.ExecutePageActionArgument> {

    @Override
    public void invoke() throws BaseException {

        if (args.getModelArgRet() == null) {
            throw new PreconditionException("args.getModelArgRet() == null");
        }

        Policy renewal = createRenewalQuotation();

        if (isPolicyBeingCreatedInTheSandpit()) {
            renewal.setTestCase(true);
        }

        // Give the current user ownership - null is okay if the user is a guest.
        renewal.setOwningUser(PageFlowContext.getRemoteUser());

        // Stop the current pageflow from trying to reinitialise the renewal policy by flagging that we've already visited.
        renewal.getPageVisit().add(new PageVisit(getPageFlowName(), getCurrentPageName(), new Date()));
    }

    private Policy createRenewalQuotation() throws BaseException {
        CreateRenewalQuotationCommand crqc = getCoreProxy().newCommand(CreateRenewalQuotationCommand.class);
        crqc.setPolicyArg((Policy)args.getModelArgRet());
        crqc.invoke();
        return crqc.getRenewalRet();
    }

    private boolean isPolicyBeingCreatedInTheSandpit() {
        return CoreContext.isPortletRequest() && CoreContext.getResponseWrapper().getNamespace().contains("_sandpit_");
    }

    protected boolean configurationSourceIsRequest() {
        return CoreContext.getPreferencesWrapper().isConfiguredByRequest();
    }

    protected boolean isNotAnAjaxRequest() {
        return !CoreContext.getRequestWrapper().isAjaxRequest();
    }
}
