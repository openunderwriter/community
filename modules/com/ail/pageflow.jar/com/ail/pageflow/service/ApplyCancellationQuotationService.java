/* Copyright Applied Industrial Logic Limited 2018. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

package com.ail.pageflow.service;

import static com.ail.core.CoreContext.getCoreProxy;

import com.ail.annotation.ServiceImplementation;
import com.ail.core.BaseException;
import com.ail.core.PreconditionException;
import com.ail.core.Service;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.quotation.ApplyCancellationQuotationService.ApplyCancellationQuotationCommand;
import com.ail.pageflow.ExecutePageActionService;

@ServiceImplementation
public class ApplyCancellationQuotationService extends Service<ExecutePageActionService.ExecutePageActionArgument> {

    @Override
    public void invoke() throws BaseException {

        if (args.getModelArgRet() == null) {
            throw new PreconditionException("args.getModelArgRet() == null");
        }

        applyCancellationQuotation((Policy)args.getModelArgRet());
    }

    private Policy applyCancellationQuotation(Policy cancellationQuotation) throws BaseException {
        ApplyCancellationQuotationCommand acqc = getCoreProxy().newCommand(ApplyCancellationQuotationCommand.class);
        acqc.setQuotationArg(cancellationQuotation);
        acqc.invoke();
        return acqc.getPolicyRet();
    }
}
