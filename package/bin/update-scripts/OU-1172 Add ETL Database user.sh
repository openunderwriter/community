#!/bin/bash

[ -z "$OU_HOME" ] && echo "OU_HOME is not set" && exit
[ -z "$DB_USERNAME" ] && echo "DB_USERNAME is not set" && exit
[ -z "$DB_PASSWORD" ] && echo "DB_PASSWORD is not set" && exit
[ -z "$DB_HOST" ] && echo "DB_HOST is not set" && exit
[ -z "$DB_PORT" ] && echo "DB_PORT is not set" && exit

set +e
mysql -u etl --password=dd7pm888ma7ftjgbian727aacnugrxtr --host=$DB_HOST --port=$DB_PORT ou_trunk_trunk_openunderwriter -Nse 'select 1' >/dev/null 2>&1
USER_DOES_NOT_EXISTS=$?
set -e

if [ "$USER_DOES_NOT_EXISTS" -eq "1" ]; then
    echo ""
    echo "OU-1172 Add ETL database user"
    echo "============================="
    echo "This update requires a manual step. Open MySQL using an administrator account, and execute the following grants:"
    echo ""
	echo "GRANT SELECT ON ou_trunk_trunk_openunderwriter.* TO 'etl'@'localhost' IDENTIFIED BY 'dd7pm888ma7ftjgbian727aacnugrxtr';"
	echo "GRANT ALL ON OU_trunk_trunk_DWH_AIL_Base_DataSource_Master_Motor.* TO 'etl'@'localhost' IDENTIFIED BY 'dd7pm888ma7ftjgbian727aacnugrxtr';"
	echo "GRANT ALL ON OU_trunk_trunk_DWH_AIL_Base_DataSource_Test.* TO 'etl'@'localhost' IDENTIFIED BY 'dd7pm888ma7ftjgbian727aacnugrxtr';"
	echo "GRANT ALL ON OU_trunk_trunk_DWH_AIL_Base_Report.* TO 'etl'@'localhost' IDENTIFIED BY 'dd7pm888ma7ftjgbian727aacnugrxtr';"
	echo "GRANT ALL ON OU_trunk_trunk_DWH_MetropolitanInsurance_Motor_Staging.* TO 'etl'@'localhost' IDENTIFIED BY 'dd7pm888ma7ftjgbian727aacnugrxtr';"
	echo "GRANT ALL ON OU_trunk_trunk_DWH_StarInsurance_Motor_Staging.* TO 'etl'@'localhost' IDENTIFIED BY 'dd7pm888ma7ftjgbian727aacnugrxtr';"
	echo ""
    
    read -p "Please enter to continue. "
fi

exit 0