package com.ail.financial.ledger;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class JournalGroupTest {
    private JournalGroup sut;

    @Mock
    Journal journal1;
    @Mock
    Journal journal2;
    @Mock
    Set<JournalGroup> journal1Groups;
    @Mock
    Set<JournalGroup> journal2Groups;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        sut = new JournalGroup();

        doReturn(journal1Groups).when(journal1).getJournalGroup();
        doReturn(journal2Groups).when(journal2).getJournalGroup();
    }

    @Test
    public void verifyThatAddSetsUpBackLink() {
        sut.add(journal1);

        assertThat(sut.getJournal(), contains(journal1));
        verify(journal1Groups).add(eq(sut));
    }

    @Test
    public void verifyThatRemoveAlsoRemovesBackLink() {
        sut.add(journal1);

        sut.remove(journal1);

        assertThat(sut.getJournal(), not(contains(journal1)));
        verify(journal1Groups).remove(eq(sut));
    }

    @Test
    public void verifyThatAddAllAlsoAddsBackLink() {
        sut.addAll(asList(journal1, journal2));

        assertThat(sut.getJournal(), hasSize(2));
        assertThat(sut.getJournal(), not(contains(journal1)));
        assertThat(sut.getJournal(), not(contains(journal2)));
        verify(journal1Groups).add(eq(sut));
        verify(journal2Groups).add(eq(sut));
    }

    @Test
    public void verifyThatRemoveAllAlsoRemovesBackLink() {
        sut.addAll(asList(journal1, journal2));

        sut.removeAll(asList(journal1, journal2));

        assertThat(sut.getJournal(), hasSize(0));
        verify(journal1Groups).remove(eq(sut));
        verify(journal2Groups).remove(eq(sut));

    }
}
