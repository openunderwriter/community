package com.ail.financial.service;

import static com.ail.financial.Currency.GBP;
import static com.ail.financial.ledger.JournalLineType.FEE;
import static com.ail.financial.ledger.JournalType.PREMIUM_ORIGINAL;
import static com.ail.financial.ledger.TransactionType.CREDIT;
import static com.ail.financial.ledger.TransactionType.DEBIT;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ail.core.BaseException;
import com.ail.core.CoreContext;
import com.ail.core.CoreProxy;
import com.ail.core.PreconditionException;
import com.ail.financial.CurrencyAmount;
import com.ail.financial.ledger.Account;
import com.ail.financial.ledger.Journal;
import com.ail.financial.ledger.JournalLine;
import com.ail.financial.ledger.Ledger;
import com.ail.financial.service.PostContraJournalService.PostContraJournalArgument;
import com.ail.financial.service.PostJournalService.PostJournalCommand;

@RunWith(PowerMockRunner.class)
@PrepareForTest({CoreContext.class, LinkJournalToAccountingPeriod.class, PostContraJournalService.class})
public class PostContraJournalServiceTest {

    private PostContraJournalService sut;

    @Mock
    private PostContraJournalArgument args;
    @Mock
    private Journal sourceJournal;
    @Mock
    private LinkJournalToAccountingPeriod linkJournalToAccountingPeriod;
    @Mock
    private CoreProxy coreProxy;
    @Mock
    private JournalLine journalLine1, journalLine2;
    @Mock
    private Ledger ledger1, ledger2;
    @Mock
    private Account account1, account2;
    @Mock
    private Date writtenDate;
    @Mock
    private PostJournalCommand postJournalCommand;
    @Captor
    private ArgumentCaptor<Journal> journalCaptor;

    private CurrencyAmount tenPounds = new CurrencyAmount(10, GBP);

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);

        sut = new PostContraJournalService();
        sut.setArgs(args);

        mockStatic(CoreContext.class);

        doReturn(writtenDate).when(args).getWrittenDateArg();
        doReturn(sourceJournal).when(args).getJournalArg();
        doReturn(false).when(sourceJournal).getHasBeenContrad();
        doReturn(PREMIUM_ORIGINAL).when(sourceJournal).getType();

        whenNew(LinkJournalToAccountingPeriod.class).withAnyArguments().thenReturn(linkJournalToAccountingPeriod);
        when(CoreContext.getCoreProxy()).thenReturn(coreProxy);
        doReturn(postJournalCommand).when(coreProxy).newCommand(eq(PostJournalCommand.class));

        doReturn(account1).when(ledger1).getAccount();
        doReturn(GBP).when(account1).getCurrency();
        doReturn(new Date(0)).when(account1).getOpeningDate();

        doReturn(account2).when(ledger2).getAccount();
        doReturn(GBP).when(account2).getCurrency();
        doReturn(new Date(0)).when(account2).getOpeningDate();

        doReturn(asList(journalLine1,journalLine2)).when(sourceJournal).getJournalLine();

        doReturn(DEBIT).when(journalLine1).getTransactionType();
        doReturn(tenPounds).when(journalLine1).getAmount();
        doReturn(ledger1).when(journalLine1).getLedger();
        doReturn(FEE).when(journalLine1).getType();

        doReturn(CREDIT).when(journalLine2).getTransactionType();
        doReturn(tenPounds).when(journalLine2).getAmount();
        doReturn(ledger2).when(journalLine2).getLedger();
        doReturn(FEE).when(journalLine2).getType();
    }

    @Test(expected = PreconditionException.class)
    public void sourceJournalCannotBeNull() throws BaseException{
        doReturn(null).when(args).getJournalArg();
        sut.invoke();
    }

    @Test(expected = PreconditionException.class)
    public void sourceJournalCannotAlreadyBeContrad() throws BaseException {
        doReturn(true).when(sourceJournal).getHasBeenContrad();
        sut.invoke();
    }

    @Test
    public void contaMustBeLinkedToSource() throws BaseException {
        sut.invoke();

        verify(postJournalCommand, atLeastOnce()).setJournalArgRet(journalCaptor.capture());
        Journal contra = journalCaptor.getAllValues().get(0);

        assertThat(contra, is(notNullValue()));
        assertThat(contra.getContraOf(), is(sourceJournal));
    }

    @Test
    public void sourceMustBeMarkerAsBeingContrad() throws BaseException {
        sut.invoke();

        verify(sourceJournal).setHasBeenContrad(eq(true));
    }

    @Test
    public void verifyLedgerAmountsAreReversed() throws BaseException {
        sut.invoke();

        verify(postJournalCommand, atLeastOnce()).setJournalArgRet(journalCaptor.capture());
        Journal contra = journalCaptor.getAllValues().get(0);

        assertThat(contra, is(notNullValue()));

        List<JournalLine> srcLines = sourceJournal.getJournalLine().stream().sorted().collect(Collectors.toList());
        List<JournalLine> tgtLines = contra.getJournalLine().stream().sorted().collect(Collectors.toList());

        assertThat(srcLines.get(0).getLedger(), is(ledger1));
        assertThat(srcLines.get(0).getTransactionType(), is(DEBIT));
        assertThat(srcLines.get(0).getAmount(), is(tenPounds));
        assertThat(srcLines.get(1).getLedger(), is(ledger2));
        assertThat(srcLines.get(1).getTransactionType(), is(CREDIT));
        assertThat(srcLines.get(1).getAmount(), is(tenPounds));

        assertThat(tgtLines.get(0).getLedger(), is(ledger2));
        assertThat(tgtLines.get(0).getTransactionType(), is(DEBIT));
        assertThat(tgtLines.get(0).getAmount(), is(tenPounds));
        assertThat(tgtLines.get(1).getLedger(), is(ledger1));
        assertThat(tgtLines.get(1).getTransactionType(), is(CREDIT));
        assertThat(tgtLines.get(1).getAmount(), is(tenPounds));
    }

    @Test
    public void verifyThatWrittenDateIsPopulated() throws BaseException {
        sut.invoke();

        verify(postJournalCommand, atLeastOnce()).setJournalArgRet(journalCaptor.capture());
        Journal contra = journalCaptor.getAllValues().get(0);

        assertThat(contra.getWrittenDate(), is(writtenDate));
    }
}
