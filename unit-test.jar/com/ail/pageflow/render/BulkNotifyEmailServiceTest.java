package com.ail.pageflow.render;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ail.core.BaseException;
import com.ail.core.CoreContext;
import com.ail.core.CoreProxy;
import com.ail.core.PreconditionException;
import com.ail.insurance.policy.Policy;
import com.ail.pageflow.render.BulkNotifyEmailService.BulkNotifyEmailArgument;
import com.ail.pageflow.render.NotifyEmailService.NotifyEmailCommand;
import com.ail.party.Party;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ CoreContext.class })
public class BulkNotifyEmailServiceTest {

    private static final String RECIPIENT = "[email address]";
    private static final String POLICY_EXTERNAL_ID = "[policy external id]";
    private static final String PARTY_EXTERNAL_ID = "[party external id]";
    private static final String TEMPLATE_NAME = "[template name]";
    private static final String FROM = "[from]";
    private static final String SUBJECT = "[subject]";
    private static final String RECIPIENT_SELECTOR = "[recipient seletor]";
    private static final String POLICY_SELECTOR_QUERY = "[dummy query]";

    private BulkNotifyEmailService sut;

    @Mock
    private BulkNotifyEmailArgument args;
    @Mock
    private CoreProxy coreProxy;
    @Mock
    private Policy policy1;
    @Mock
    private Party party;
    @Mock
    NotifyEmailCommand notifyEmailCommand;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        sut = new BulkNotifyEmailService();
        sut.setArgs(args);

        mockStatic(CoreContext.class);
        when(CoreContext.getCoreProxy()).thenReturn(coreProxy);

        doReturn(POLICY_SELECTOR_QUERY).when(args).getEntitySelectorQueryArg();
        doReturn(RECIPIENT_SELECTOR).when(args).getRecipientSelectorArg();
        doReturn(SUBJECT).when(args).getSubjectArg();
        doReturn(FROM).when(args).getFromArg();
        doReturn(TEMPLATE_NAME).when(args).getTemplateNameArg();
        doReturn(null).when(args).getTextArg();

        doReturn(asList(policy1)).when(coreProxy).query(eq(POLICY_SELECTOR_QUERY));
        doReturn(POLICY_EXTERNAL_ID).when(policy1).getExternalSystemId();

        doReturn(party).when(policy1).xpathGet(eq(RECIPIENT_SELECTOR), eq(null), eq(Party.class));
        doReturn(PARTY_EXTERNAL_ID).when(party).getExternalSystemId();
        doReturn(RECIPIENT).when(party).getEmailAddress();

        doReturn(notifyEmailCommand).when(coreProxy).newCommand(eq(NotifyEmailCommand.class));
    }

    @Test(expected = PreconditionException.class)
    public void verifyThatNullQueryIsTrapped() throws BaseException {
        doReturn(null).when(args).getEntitySelectorQueryArg();
        sut.invoke();
    }

    @Test(expected = PreconditionException.class)
    public void verifyThatEmptyQueryIsTrapped() throws BaseException {
        doReturn("").when(args).getEntitySelectorQueryArg();
        sut.invoke();
    }

    @Test(expected = PreconditionException.class)
    public void verifyThatNullRecipientTrapped() throws BaseException {
        doReturn(null).when(args).getRecipientSelectorArg();
        sut.invoke();
    }

    @Test(expected = PreconditionException.class)
    public void verifyThatEmptyRecipientIsTrapped() throws BaseException {
        doReturn("").when(args).getRecipientSelectorArg();
        sut.invoke();
    }

    @Test(expected = PreconditionException.class)
    public void verifyThatNullSubjectTrapped() throws BaseException {
        doReturn(null).when(args).getSubjectArg();
        sut.invoke();
    }

    @Test(expected = PreconditionException.class)
    public void verifyThatEmptySubjectIsTrapped() throws BaseException {
        doReturn("").when(args).getSubjectArg();
        sut.invoke();
    }

    @Test(expected = PreconditionException.class)
    public void verifyThatNullFromTrapped() throws BaseException {
        doReturn(null).when(args).getFromArg();
        sut.invoke();
    }

    @Test(expected = PreconditionException.class)
    public void verifyThatEmptyFromIsTrapped() throws BaseException {
        doReturn("").when(args).getFromArg();
        sut.invoke();
    }

    @Test(expected = PreconditionException.class)
    public void verifyThatBothTemplateAndTextCannotBeEmpty() throws BaseException {
        doReturn("").when(args).getTemplateNameArg();
        doReturn("").when(args).getTextArg();
        sut.invoke();
    }

    @Test(expected = PreconditionException.class)
    public void verifyThatBothTemplateAndCannotBePopulated() throws BaseException {
        doReturn("test").when(args).getTemplateNameArg();
        doReturn("test").when(args).getTextArg();
        sut.invoke();
    }

    @Test
    public void confirmQueryIsExecuted() throws BaseException {
        sut.invoke();

        verify(coreProxy).query(eq(POLICY_SELECTOR_QUERY));
    }


    @Test(expected = PreconditionException.class)
    public void confirmQueryReturningNonPolicyEntitiesIsTrapped() throws BaseException {
        // Using Serializable here, but anything that doesn't extend com.ail.core.Type would cause the exception too.
        doReturn(asList(mock(Serializable.class))).when(coreProxy).query(eq(POLICY_SELECTOR_QUERY));

        sut.invoke();
    }

    @Test
    public void testBadRecipientSelector() throws BaseException {
        doReturn(null).when(policy1).xpathGet(eq(RECIPIENT_SELECTOR), eq(null), eq(Party.class));

        List<String> results = new ArrayList<>();
        doReturn(results).when(args).getResultsRet();

        sut.invoke();

        assertThat(args.getResultsRet(), hasSize(1));
        assertThat(args.getResultsRet().get(0), containsString("FAILURE"));
        assertThat(args.getResultsRet().get(0), containsString(POLICY_EXTERNAL_ID));
        assertThat(args.getResultsRet().get(0), containsString(RECIPIENT_SELECTOR));
    }

    @Test
    public void testRecipientWithoutEmail() throws BaseException {
        doReturn(null).when(party).getEmailAddress();

        List<String> results = new ArrayList<>();
        doReturn(results).when(args).getResultsRet();

        sut.invoke();

        assertThat(args.getResultsRet(), hasSize(1));
        assertThat(args.getResultsRet().get(0), containsString("FAILURE"));
        assertThat(args.getResultsRet().get(0), containsString(POLICY_EXTERNAL_ID));
        assertThat(args.getResultsRet().get(0), containsString(PARTY_EXTERNAL_ID));
        assertThat(args.getResultsRet().get(0), containsString("has an empty email adrress"));
    }

    @Test
    public void testHappyPath() throws BaseException {
        List<String> results = new ArrayList<>();
        doReturn(results).when(args).getResultsRet();

        sut.invoke();

        assertThat(args.getResultsRet(), hasSize(1));
        assertThat(args.getResultsRet().get(0), containsString("SUCCESS"));
        assertThat(args.getResultsRet().get(0), containsString(POLICY_EXTERNAL_ID));
        assertThat(args.getResultsRet().get(0), containsString(RECIPIENT));

        verify(notifyEmailCommand).setFromArg(eq(FROM));
        verify(notifyEmailCommand).setPolicyArg(eq(policy1));
        verify(notifyEmailCommand).setSubjectArg(eq(SUBJECT));
        verify(notifyEmailCommand).setTemplateNameArg(eq(TEMPLATE_NAME));
        verify(notifyEmailCommand).setToArg(eq(RECIPIENT));
        verify(notifyEmailCommand).invoke();
    }

}
