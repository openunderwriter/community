package com.ail.insurance.quotation;

import static com.ail.insurance.policy.PolicyLinkType.RENEWAL_QUOTATION_FOR;
import static com.ail.insurance.policy.PolicyLinkType.RENEWAL_QUOTATION_FROM;
import static com.ail.insurance.policy.PolicyStatus.APPLICATION;
import static com.ail.insurance.policy.PolicyStatus.NOT_TAKEN_UP;
import static com.ail.insurance.policy.PolicyStatus.ON_RISK;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ail.core.BaseException;
import com.ail.core.Core;
import com.ail.core.CoreContext;
import com.ail.core.CoreProxy;
import com.ail.core.PreconditionException;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.policy.PolicyLink;
import com.ail.insurance.policy.PolicyStatus;
import com.ail.insurance.quotation.CreateRenewalQuotationService.CreateRenewalQuotationArgument;
import com.ail.insurance.quotation.InitialiseRenewalService.InitialiseRenewalCommand;
import com.ail.insurance.quotation.PrepareRequoteService.PrepareRequoteCommand;

@RunWith(PowerMockRunner.class)
@PrepareForTest({CoreContext.class, CreateRenewalQuotationService.class})
public class CreateRenewalQuotationServiceTest {

    private static final long RENEWAL_SYSTEM_ID = 321L;

    private static final long POLICY_SYSTEM_ID = 1234L;

    private CreateRenewalQuotationService sut;

    @Mock
    private CreateRenewalQuotationArgument args;
    @Mock
    private Policy policy;
    @Mock
    private Policy renewal;
    @Mock
    private PolicyLink renewalLink;
    @Mock
    private PolicyLink mtaLink;
    @Mock
    private CoreProxy coreProxy;
    @Mock
    private Core core;
    @Mock
    private PrepareRequoteCommand prepareRequoteCommand;
    @Mock
    private InitialiseRenewalCommand initialiseRenewalCommand;

    private List<PolicyLink> policyPolicyLinks = new ArrayList<>();

    private List<PolicyLink> renewalPolicyLinks = new ArrayList<>();


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        mockStatic(CoreContext.class);

        sut = spy(new CreateRenewalQuotationService());
        sut.setArgs(args);

        doReturn(core).when(sut).getCore();

        doReturn(policy).when(args).getPolicyArg();
        doReturn(ON_RISK).when(policy).getStatus();
        doReturn(POLICY_SYSTEM_ID).when(policy).getSystemId();
        doReturn(renewal).when(args).getRenewalRet();
        doReturn(renewal).when(coreProxy).create(eq(renewal));

        when(CoreContext.getCoreProxy()).thenReturn(coreProxy);

        doReturn(prepareRequoteCommand).when(coreProxy).newCommand(eq(PrepareRequoteCommand.class));
        doReturn(renewal).when(prepareRequoteCommand).getRequoteRet();
        doReturn(RENEWAL_SYSTEM_ID).when(renewal).getSystemId();

        doReturn(initialiseRenewalCommand).when(coreProxy).newCommand(eq("InitialiseRenewal"), eq(InitialiseRenewalCommand.class));

        doReturn(policyPolicyLinks).when(policy).getPolicyLink();
        doReturn(renewalPolicyLinks).when(renewal).getPolicyLink();
    }

    @Test(expected = PreconditionException.class)
    public void confirmPolicyArgCannotBeNul() throws BaseException {
        doReturn(null).when(args).getPolicyArg();
        sut.invoke();
    }

    @Test
    public void confirmPolicyArgStatusCannotBeValuesOtherThanOnRisk() throws BaseException {
        Stream.of(PolicyStatus.values()).
               filter(s -> s != ON_RISK).
               forEach(status -> {
                   doReturn(status).when(policy).getStatus();
                   try {
                       sut.invoke();
                       fail("PolicyStatus: '"+status+"' was accepted.");
                    } catch (BaseException e) {
                        // ignore this, it is what we want.
                    }
               });
    }

    @Test
    public void confirmThatExistingRenewalQuotationsAreMarkedAsNotTakenUp() throws BaseException {
        Long policySystemIdForRenewalLink = 123L;

        PolicyLink renewalLink = mock(PolicyLink.class);
        doReturn(policySystemIdForRenewalLink).when(renewalLink).getTargetPolicyId();
        doReturn(RENEWAL_QUOTATION_FOR).when(renewalLink).getLinkType();
        policyPolicyLinks.add(renewalLink);

        Policy linkedPolicy = mock(Policy.class);
        doReturn(linkedPolicy).when(coreProxy).queryUnique(eq("get.policy.by.systemId"), eq(policySystemIdForRenewalLink));

        sut.invoke();

        verify(linkedPolicy).setStatus(eq(NOT_TAKEN_UP));
    }

    @Test
    public void confirmPrepareRequoteServiceArgsAreCorrect() throws BaseException {
        sut.invoke();

        verify(prepareRequoteCommand).setPolicyArg(eq(policy));
        verify(prepareRequoteCommand).setSuppressDocumentCopyArg(eq(true));
        verify(prepareRequoteCommand).invoke();
    }

    @Test
    public void conformThatRequotePolicyIsReturned() throws BaseException {
        sut.invoke();
        verify(args).setRenewalRet(eq(renewal));
    }

    @Test
    public void confirmPolicyLinksAreCorrectlySetup() throws BaseException {
        sut.invoke();

        assertThat(policyPolicyLinks, contains(new PolicyLink(RENEWAL_QUOTATION_FOR, RENEWAL_SYSTEM_ID)));
        assertThat(renewalPolicyLinks, contains(new PolicyLink(RENEWAL_QUOTATION_FROM, POLICY_SYSTEM_ID)));
    }

    @Test
    public void confirmInitialiseRenewalCommandIsInvoked() throws BaseException {
        sut.invoke();
        verify(initialiseRenewalCommand).invoke();
    }

    @Test
    public void confirmQuotationStatusIsSetToApplication() throws BaseException {
        sut.invoke();
        verify(renewal).setStatus(eq(APPLICATION));
    }

}
