package com.ail.insurance.quotation;

import static com.ail.insurance.policy.PolicyLinkType.DISAGGREGATED_TO;
import static com.ail.insurance.policy.PolicyLinkType.RENEWAL_QUOTATION_FROM;
import static com.ail.insurance.policy.PolicyStatus.APPLICATION;
import static com.ail.insurance.policy.PolicyStatus.APPLIED;
import static com.ail.insurance.policy.PolicyStatus.ON_RISK;
import static com.ail.insurance.policy.PolicyStatus.SUBMITTED;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ail.core.BaseException;
import com.ail.core.CoreContext;
import com.ail.core.CoreProxy;
import com.ail.core.PreconditionException;
import com.ail.core.document.Document;
import com.ail.core.document.DocumentContent;
import com.ail.financial.PaymentRecord;
import com.ail.financial.PaymentSchedule;
import com.ail.insurance.policy.AssessmentSheet;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.policy.PolicyLink;
import com.ail.insurance.policy.PolicyStatus;
import com.ail.insurance.policy.Section;
import com.ail.insurance.quotation.ApplyRenewalQuotationService.ApplyRenewalQuotationArgument;
import com.ail.insurance.quotation.FinaliseRenewalService.FinaliseRenewalCommand;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ CoreContext.class, ApplyRenewalQuotationService.class })
public class ApplyRenewalQuotationServiceTest {

    private static final String SECTION_TYPE_ID = "SECTION_TYPE_ID";

    private static final String SECTION_ID = "SECTION_ID";

    private static final Long RENEWING_POLICY_SYSTEM_ID = 1234L;

    private ApplyRenewalQuotationService sut;

    @Mock
    private ApplyRenewalQuotationArgument args;
    @Mock
    private Policy quotationPolicy;
    @Mock
    private Policy renewingPolicy;
    @Mock
    private CoreProxy coreProxy;
    @Mock
    private FinaliseRenewalCommand finaliseRenewalCommand;
    @Mock
    Map<String, AssessmentSheet> assessmentSheetList;
    @Mock
    Map<String, AssessmentSheet> clonedAssessmentSheetList;
    @Mock
    private PaymentRecord quotationPaymentRecord;
    @Mock
    private PaymentRecord parentPaymentRecord;

    private List<PolicyLink> quotationPolicyLinks = new ArrayList<>();
    private List<PaymentRecord> quotationPaymentHistory = new ArrayList<>();
    private List<PaymentRecord> parentPaymentHistory = new ArrayList<>();

    @Before
    public void setup() throws CloneNotSupportedException {
        MockitoAnnotations.initMocks(this);

        mockStatic(CoreContext.class);
        when(CoreContext.getCoreProxy()).thenReturn(coreProxy);

        sut = spy(new ApplyRenewalQuotationService());
        sut.setArgs(args);

        doReturn(quotationPolicy).when(args).getQuotationArg();
        doReturn(SUBMITTED).when(quotationPolicy).getStatus();
        doReturn(quotationPolicyLinks).when(quotationPolicy).getPolicyLink();

        quotationPolicyLinks.add(new PolicyLink(RENEWAL_QUOTATION_FROM, RENEWING_POLICY_SYSTEM_ID));

        doReturn(renewingPolicy).when(coreProxy).queryUnique(eq("get.policy.by.systemId"), eq(RENEWING_POLICY_SYSTEM_ID));
        doReturn(ON_RISK).when(renewingPolicy).getStatus();

        doReturn(finaliseRenewalCommand).when(coreProxy).newCommand("FinaliseRenewal", FinaliseRenewalCommand.class);

        doReturn(quotationPaymentHistory).when(quotationPolicy).getPaymentHistory();
        doReturn(parentPaymentHistory).when(renewingPolicy).getPaymentHistory();

        doReturn(quotationPaymentRecord).when(quotationPaymentRecord).clone();

        quotationPaymentHistory.add(quotationPaymentRecord);
        parentPaymentHistory.add(parentPaymentRecord);
}

    @Test(expected = PreconditionException.class)
    public void quotationArgMustBePopulated() throws BaseException {
        doReturn(null).when(args).getQuotationArg();
        sut.invoke();
    }

    @Test
    public void confirmQuotationArgStatusCannotBeValuesOtherThanSubmitted() throws BaseException {
        Stream.of(PolicyStatus.values()).
               filter(s -> s != PolicyStatus.SUBMITTED).
               forEach(status -> {
                   doReturn(status).when(quotationPolicy).getStatus();
                   try {
                       sut.invoke();
                       fail("PolicyStatus: '"+status+"' was accepted.");
                    } catch (BaseException e) {
                        // ignore this, it is what we want.
                    }
               });
    }

    @Test(expected = PreconditionException.class)
    public void whenNoRenewalLinksAreFoundAnExceptionIsThrown() throws BaseException {
        quotationPolicyLinks.clear();
        quotationPolicyLinks.add(new PolicyLink(DISAGGREGATED_TO, RENEWING_POLICY_SYSTEM_ID));
        sut.invoke();
    }

    @Test(expected = PreconditionException.class)
    public void whenMoreThanOneenewalLinkIsFoundAnExceptionIsThrown() throws BaseException {
        quotationPolicyLinks.clear();
        quotationPolicyLinks.add(new PolicyLink(RENEWAL_QUOTATION_FROM, 1L));
        quotationPolicyLinks.add(new PolicyLink(RENEWAL_QUOTATION_FROM, 2L));
        sut.invoke();
    }

    @Test(expected = PreconditionException.class)
    public void ifRenewingPolicyCannotBeFoundAnExceptionIsThrown() throws BaseException {
        doReturn(null).when(coreProxy).queryUnique(eq("get.policy.by.systemId"), eq(RENEWING_POLICY_SYSTEM_ID));
        sut.invoke();
    }

    @Test(expected = PreconditionException.class)
    public void ifRenewingPolicyIsNotOnRiskAnExceptionIsThrown() throws BaseException {
        doReturn(APPLICATION).when(renewingPolicy).getStatus();
        sut.invoke();
    }

    @Test
    public void confirmThatDatesAreTransfered() throws BaseException {
        Date inception = mock(Date.class);
        Date expiry = mock(Date.class);

        doReturn(inception).when(quotationPolicy).getInceptionDate();
        doReturn(expiry).when(quotationPolicy).getExpiryDate();

        sut.invoke();

        verify(renewingPolicy).setInceptionDate(eq(inception));
        verify(renewingPolicy).setExpiryDate(eq(expiry));
    }

    @Test
    public void confirmThatQuotationDocumentIsTransfered() throws BaseException {
        byte[] content = "TEST".getBytes();
        Document document = mock(Document.class);
        DocumentContent documentContent = mock(DocumentContent.class);

        doReturn(content).when(documentContent).getContent();
        doReturn(documentContent).when(document).getDocumentContent();
        doReturn(document).when(quotationPolicy).retrieveQuotationDocument();

        sut.invoke();

        verify(renewingPolicy).attachQuotationDocument(eq(content));
    }

    @Test
    public void confirmThatPolicyLevelAssessmentSheetsIsTransfered() throws BaseException {

        doReturn(assessmentSheetList).when(quotationPolicy).getAssessmentSheetList();
        doReturn(clonedAssessmentSheetList).when(sut).cloneAssessmentSheets(eq(assessmentSheetList));

        sut.invoke();

        verify(renewingPolicy).setAssessmentSheetList(eq(clonedAssessmentSheetList));
    }

    @Test
    public void confirmThatSectionLevelAssessmentSheetsIsTransfered() throws BaseException {

        Section quotationSection = mock(Section.class);
        Section renewalSection = mock(Section.class);

        doReturn(SECTION_ID).when(quotationSection).getId();
        doReturn(SECTION_ID).when(renewalSection).getId();
        doReturn(SECTION_TYPE_ID).when(quotationSection).getSectionTypeId();
        doReturn(SECTION_TYPE_ID).when(renewalSection).getSectionTypeId();

        doReturn(assessmentSheetList).when(quotationSection).getAssessmentSheetList();
        doReturn(clonedAssessmentSheetList).when(sut).cloneAssessmentSheets(eq(assessmentSheetList));

        doReturn(asList(quotationSection)).when(quotationPolicy).getSection();
        doReturn(asList(renewalSection)).when(renewingPolicy).getSection();

        doReturn(renewalSection).when(renewingPolicy).getSectionById(SECTION_ID);

        sut.invoke();

        verify(renewalSection).setAssessmentSheetList(eq(clonedAssessmentSheetList));
    }

    @Test
    public void confirmPaymentDetailsIsTransfered() throws BaseException {
        PaymentSchedule paymentSchedule = mock(PaymentSchedule.class);

        doReturn(paymentSchedule).when(quotationPolicy).getPaymentDetails();

        sut.invoke();

        verify(renewingPolicy).setPaymentDetails(eq(paymentSchedule));
    }

    @Test
    public void confirmThatRenewalIndexIsIncremented() throws BaseException {
        doReturn(21L).when(renewingPolicy).getRenewalIndex();

        sut.invoke();

        verify(renewingPolicy).setRenewalIndex(eq(22L));
    }

    @Test
    public void confirmThatQuotationsIsMarkedAsApplied() throws BaseException {
        sut.invoke();

        verify(quotationPolicy).setStatus(eq(APPLIED));
    }

    @Test
    public void confirmThatFinaliseRenewalCommandIsInvoked() throws BaseException {
        sut.invoke();

        verify(finaliseRenewalCommand).setRenewalQuotationArg(eq(quotationPolicy));
        verify(finaliseRenewalCommand).setRenewingPolicyArg(eq(renewingPolicy));
        verify(finaliseRenewalCommand).invoke();
    }

    @Test
    public void confirmPaymentHistortyIsCopiedToParent() throws BaseException {
        sut.invoke();

        assertThat(parentPaymentHistory, hasSize(2));
        assertThat(parentPaymentHistory, containsInAnyOrder(parentPaymentRecord, quotationPaymentRecord));
    }
}
