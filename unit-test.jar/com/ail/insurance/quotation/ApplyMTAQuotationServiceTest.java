package com.ail.insurance.quotation;

import static com.ail.insurance.policy.PolicyLinkType.DISAGGREGATED_TO;
import static com.ail.insurance.policy.PolicyLinkType.MTA_QUOTATION_FROM;
import static com.ail.insurance.policy.PolicyStatus.APPLICATION;
import static com.ail.insurance.policy.PolicyStatus.APPLIED;
import static com.ail.insurance.policy.PolicyStatus.ON_RISK;
import static com.ail.insurance.policy.PolicyStatus.SUBMITTED;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ail.core.BaseException;
import com.ail.core.CoreContext;
import com.ail.core.CoreProxy;
import com.ail.core.PreconditionException;
import com.ail.core.document.Document;
import com.ail.core.document.DocumentContent;
import com.ail.financial.PaymentRecord;
import com.ail.financial.PaymentSchedule;
import com.ail.insurance.policy.AssessmentSheet;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.policy.PolicyLink;
import com.ail.insurance.policy.PolicyStatus;
import com.ail.insurance.policy.Section;
import com.ail.insurance.quotation.ApplyMTAQuotationService.ApplyMTAQuotationArgument;
import com.ail.insurance.quotation.FinaliseMTAService.FinaliseMTACommand;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ CoreContext.class, ApplyMTAQuotationService.class })
public class ApplyMTAQuotationServiceTest {

    private static final String SECTION_TYPE_ID = "SECTION_TYPE_ID";
    private static final String SECTION_ID = "SECTION_ID";
    private static final Long MASTER_POLICY_SYSTEM_ID = 1234L;

    private ApplyMTAQuotationService sut;

    @Mock
    private ApplyMTAQuotationArgument args;
    @Mock
    private Policy quotationPolicy;
    @Mock
    private Policy masterPolicy;
    @Mock
    private CoreProxy coreProxy;
    @Mock
    private FinaliseMTACommand finaliseMTACommand;
    @Mock
    private Map<String, AssessmentSheet> assessmentSheetList;
    @Mock
    private Map<String, AssessmentSheet> clonedAssessmentSheetList;
    @Mock
    private PaymentRecord quotationPaymentRecord;
    @Mock
    private PaymentRecord parentPaymentRecord;

    private List<PolicyLink> quotationPolicyLinks = new ArrayList<>();
    private List<PaymentRecord> quotationPaymentHistory = new ArrayList<>();
    private List<PaymentRecord> parentPaymentHistory = new ArrayList<>();

    @Before
    public void setup() throws CloneNotSupportedException {
        MockitoAnnotations.initMocks(this);

        mockStatic(CoreContext.class);
        when(CoreContext.getCoreProxy()).thenReturn(coreProxy);

        sut = spy(new ApplyMTAQuotationService());
        sut.setArgs(args);

        doReturn(quotationPolicy).when(args).getQuotationArg();
        doReturn(SUBMITTED).when(quotationPolicy).getStatus();
        doReturn(quotationPolicyLinks).when(quotationPolicy).getPolicyLink();

        quotationPolicyLinks.add(new PolicyLink(MTA_QUOTATION_FROM, MASTER_POLICY_SYSTEM_ID));

        doReturn(masterPolicy).when(coreProxy).queryUnique(eq("get.policy.by.systemId"), eq(MASTER_POLICY_SYSTEM_ID));
        doReturn(ON_RISK).when(masterPolicy).getStatus();

        doReturn(finaliseMTACommand).when(coreProxy).newCommand("FinaliseMTA", FinaliseMTACommand.class);

        doReturn(quotationPaymentHistory).when(quotationPolicy).getPaymentHistory();
        doReturn(parentPaymentHistory).when(masterPolicy).getPaymentHistory();

        doReturn(quotationPaymentRecord).when(quotationPaymentRecord).clone();

        quotationPaymentHistory.add(quotationPaymentRecord);
        parentPaymentHistory.add(parentPaymentRecord);
    }

    @Test(expected = PreconditionException.class)
    public void quotationArgMustBePopulated() throws BaseException {
        doReturn(null).when(args).getQuotationArg();
        sut.invoke();
    }

    @Test
    public void confirmQuotationArgStatusCannotBeValuesOtherThanSubmitted() throws BaseException {
        Stream.of(PolicyStatus.values()).
               filter(s -> s != PolicyStatus.SUBMITTED).
               forEach(status -> {
                   doReturn(status).when(quotationPolicy).getStatus();
                   try {
                       sut.invoke();
                       fail("PolicyStatus: '"+status+"' was accepted.");
                    } catch (BaseException e) {
                        // ignore this, it is what we want.
                    }
               });
    }

    @Test(expected = PreconditionException.class)
    public void whenNoMTALinksAreFoundAnExceptionIsThrown() throws BaseException {
        quotationPolicyLinks.clear();
        quotationPolicyLinks.add(new PolicyLink(DISAGGREGATED_TO, MASTER_POLICY_SYSTEM_ID));
        sut.invoke();
    }

    @Test(expected = PreconditionException.class)
    public void whenMoreThanOneMTALinkIsFoundAnExceptionIsThrown() throws BaseException {
        quotationPolicyLinks.clear();
        quotationPolicyLinks.add(new PolicyLink(MTA_QUOTATION_FROM, 1L));
        quotationPolicyLinks.add(new PolicyLink(MTA_QUOTATION_FROM, 2L));
        sut.invoke();
    }

    @Test(expected = PreconditionException.class)
    public void ifMasterPolicyCannotBeFoundAnExceptionIsThrown() throws BaseException {
        doReturn(null).when(coreProxy).queryUnique(eq("get.policy.by.systemId"), eq(MASTER_POLICY_SYSTEM_ID));
        sut.invoke();
    }

    @Test(expected = PreconditionException.class)
    public void ifMasterPolicyIsNotOnRiskAnExceptionIsThrown() throws BaseException {
        doReturn(APPLICATION).when(masterPolicy).getStatus();
        sut.invoke();
    }

    @Test
    public void confirmThatDatesAreTransfered() throws BaseException {
        Date inception = mock(Date.class);
        Date expiry = mock(Date.class);

        doReturn(inception).when(quotationPolicy).getInceptionDate();
        doReturn(expiry).when(quotationPolicy).getExpiryDate();

        sut.invoke();

        verify(masterPolicy, never()).setInceptionDate(any(Date.class));
        verify(masterPolicy).setInforceDate(eq(inception));
        verify(masterPolicy).setExpiryDate(eq(expiry));
    }

    @Test
    public void confirmThatQuotationDocumentIsTransfered() throws BaseException {
        byte[] content = "TEST".getBytes();
        Document document = mock(Document.class);
        DocumentContent documentContent = mock(DocumentContent.class);

        doReturn(content).when(documentContent).getContent();
        doReturn(documentContent).when(document).getDocumentContent();
        doReturn(document).when(quotationPolicy).retrieveQuotationDocument();

        sut.invoke();

        verify(masterPolicy).attachQuotationDocument(eq(content));
    }

    @Test
    public void confirmThatPolicyLevelAssessmentSheetsIsTransfered() throws BaseException {
        doReturn(assessmentSheetList).when(quotationPolicy).getAssessmentSheetList();
        doReturn(clonedAssessmentSheetList).when(sut).cloneAssessmentSheets(eq(assessmentSheetList));

        sut.invoke();

        verify(masterPolicy).setAssessmentSheetList(eq(clonedAssessmentSheetList));
    }

    @Test
    public void confirmThatSectionLevelAssessmentSheetsIsTransfered() throws BaseException {

        Section quotationSection = mock(Section.class);
        Section masterSection = mock(Section.class);

        doReturn(SECTION_ID).when(quotationSection).getId();
        doReturn(SECTION_ID).when(masterSection).getId();
        doReturn(SECTION_TYPE_ID).when(quotationSection).getSectionTypeId();
        doReturn(SECTION_TYPE_ID).when(masterSection).getSectionTypeId();

        doReturn(assessmentSheetList).when(quotationSection).getAssessmentSheetList();
        doReturn(clonedAssessmentSheetList).when(sut).cloneAssessmentSheets(eq(assessmentSheetList));

        doReturn(asList(quotationSection)).when(quotationPolicy).getSection();
        doReturn(asList(masterSection)).when(masterPolicy).getSection();

        doReturn(masterSection).when(masterPolicy).getSectionById(SECTION_ID);

        sut.invoke();

        verify(masterSection).setAssessmentSheetList(eq(clonedAssessmentSheetList));
    }

    @Test
    public void confirmPaymentDetailsIsTransfered() throws BaseException {
        PaymentSchedule paymentSchedule = mock(PaymentSchedule.class);

        doReturn(paymentSchedule).when(quotationPolicy).getPaymentDetails();

        sut.invoke();

        verify(masterPolicy).setPaymentDetails(eq(paymentSchedule));
    }

    @Test
    public void confirmThatMTAIndexIsIncremented() throws BaseException {
        doReturn(21L).when(masterPolicy).getMtaIndex();

        sut.invoke();

        verify(masterPolicy).setMtaIndex(eq(22L));
    }

    @Test
    public void confirmThatQuotationsIsMarkedAsApplied() throws BaseException {
        sut.invoke();

        verify(quotationPolicy).setStatus(eq(APPLIED));
    }

    @Test
    public void confirmThatFinaliseMTACommandIsInvoked() throws BaseException {
        sut.invoke();

        verify(finaliseMTACommand).setQuotationArg(eq(quotationPolicy));
        verify(finaliseMTACommand).setPolicyArg(eq(masterPolicy));
        verify(finaliseMTACommand).invoke();
    }

    @Test
    public void confirmPaymentHistortyIsCopiedToParent() throws BaseException {
        sut.invoke();

        assertThat(parentPaymentHistory, hasSize(2));
        assertThat(parentPaymentHistory, containsInAnyOrder(parentPaymentRecord, quotationPaymentRecord));
    }
}
