package com.ail.core.document;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

public class DocumentTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void cloneMustWorkWhenContentPropertyIsUninitiliased() {
        Document document = new Document("TYPE", (String) null, "TITLE", "FILENAME", "MIME_TYPE", "product-type-id");

        try {
            document.clone();
        } catch (Exception e) {
            fail("Exception thrown by clone()"+e);
        }
    }

    @Test
    public void jpgMimeTypeMustTransalte() {
        Document document = new Document("TYPE", (String) null, "TITLE", "FILENAME", "image/jpg", "product-type-id");

        assertEquals("image/jpeg", document.getMimeType());
    }

    @Test
    public void nonJpgMimeTypeMustNotTransalte() {
        Document document = new Document("TYPE", (String) null, "TITLE", "FILENAME", "image/png", "product-type-id");

        assertEquals("image/png", document.getMimeType());
    }

    @Test
    public void testDocumentCompareTo() {
        Document d1, d2;
        Date now = new Date();

        d1 = new Document("aType", (String) null, "TITLE", "FILENAME", "image/png", "product-type-id");
        d2 = new Document("bType", (String) null, "TITLE", "FILENAME", "image/png", "product-type-id");

        assertThat(d1.compareTo(d2), is(lessThan(0)));
        assertThat(d2.compareTo(d1), is(greaterThan(0)));

        d1 = new Document("aType", (String) null, "TITLE", "FILENAME", "image/png", "product-type-id");
        d2 = new Document("aType", (String) null, "TITLE", "FILENAME", "image/png", "product-type-id");

        assertThat(d1.compareTo(d2), is(0));

        d1 = spy(d1);
        when(d1.getCreatedDate()).thenReturn(now);
        d2 = spy(d2);
        when(d2.getCreatedDate()).thenReturn(new Date(now.getTime() + 2000));

        assertThat(d1.compareTo(d2), is(lessThan(0)));
        assertThat(d2.compareTo(d1), is(greaterThan(0)));
    }
}
