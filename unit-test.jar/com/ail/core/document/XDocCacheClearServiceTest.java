package com.ail.core.document;

import static java.util.Arrays.asList;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ail.core.document.XDocCacheClearService.XDocCacheClearArgument;

import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;

@RunWith(PowerMockRunner.class)
@PrepareForTest({XDocCacheClearServiceTest.class, XDocReportRegistry.class})
public class XDocCacheClearServiceTest {

    private static final String DUMMY_NAMESPACE = "DUMMY.NAMESPACE.Registry";
    private static final String MATCHING_REPORT_ID = "/DUMMY/NAMESPACE/File.xdoc";
    private static final String NON_MATCHING_REPORT_ID = "/TEST/TEST/File.xdoc";

    private XDocCacheClearService sut;

    @Mock
    private XDocCacheClearArgument args;
    @Mock
    private XDocReportRegistry xdocReportRegistry;
    @Mock
    private IXDocReport report1;
    @Mock
    private IXDocReport report2;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        sut = new XDocCacheClearService();
        sut.setArgs(args);

        doReturn(DUMMY_NAMESPACE).when(args).getProductNamespaceArg();

        mockStatic(XDocReportRegistry.class);

        when(XDocReportRegistry.getRegistry()).thenReturn(xdocReportRegistry);
        xdocReportRegistry.getCachedReports();
        doReturn(asList(report1, report2)).when(xdocReportRegistry).getCachedReports();

        doReturn(MATCHING_REPORT_ID).when(report1).getId();
        doReturn(NON_MATCHING_REPORT_ID).when(report2).getId();
    }

    @Test
    public void verifyThatEmptyArgTriggersClear() {
        doReturn(null).when(args).getProductNamespaceArg();

        sut.invoke();

        verify(xdocReportRegistry).clear();
        verify(xdocReportRegistry, never()).unregisterReport(any(IXDocReport.class));
    }

    @Test
    public void verifyThatNamespaceArgTriggersSpecificClear() {

        sut.invoke();

        verify(xdocReportRegistry, never()).clear();
        verify(xdocReportRegistry).unregisterReport(eq(report1));
        verify(xdocReportRegistry, never()).unregisterReport(eq(report2));
    }
}
