package com.ail.base;
import com.ail.core.product.ProductServiceCommand;
import com.ail.insurance.quotation.FinaliseRenewalService.FinaliseRenewalArgument;

/**
 * Apply the changes made in a renewal quotation to the master policy.
 * <p>
 * This default implementation does nothing as the core system's
 * {@link com.ail.insurance.quotation.ApplyRenewalQuotationService ApplyRenewalQuotationService} has already taken care of all the
 * standard merging steps before it invokes this product specific service.
 * Product specific implementation of this service are expected to deal with
 * updating the master policy's assets, sections, coverages etc. wrt the renewal
 * quotation.
 * </p>
 * <p>
 * Essentially, product specific implementation will update
 * {@link FinaliseRenewalArgument#getRenewingPolicyArg()} with respect to values
 * taken from {@link FinaliseRenewalArgument#getRenewalQuotationArg()}.
 * </p>
 */
@ProductServiceCommand(serviceName = "FinaliseRenewalService", commandName = "FinaliseRenewal")
public class FinaliseRenewalService {

	public static void invoke(FinaliseRenewalArgument args) {
	}
}