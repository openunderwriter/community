package com.ail.base;

import com.ail.core.product.ProductServiceCommand;
import com.ail.insurance.quotation.FinaliseCancellationService.FinaliseCancellationArgument;

/**
 * Apply the changes made in an Cancellation quotation to the master policy.
 * <p>
 * This default implementation does nothing as the core system's
 * {@link com.ail.insurance.quotation.ApplyCancellationQuotationService
 * ApplyCancellationQuotationService} has already taken care of all the standard merging
 * steps before it invokes this product specific service. Product specific
 * implementation of this service are expected to deal with updating the master
 * policy's assets, sections, coverages etc. wrt the Cancellation quotation.
 * </p>
 * <p>
 * Essentially, product specific implementation will update
 * {@link FinaliseCancellationArgument#getCancellationPolicyArg()} with respect to values taken
 * from {@link FinaliseCancellationArgument#getCancellationQuotationArg()}.
 * </p>
 */
@ProductServiceCommand(serviceName = "FinaliseCancellationService", commandName = "FinaliseCancellation")
public class FinaliseCancellationService {

	public static void invoke(FinaliseCancellationArgument args) {
	}
}