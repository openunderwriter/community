/* Copyright Applied Industrial Logic Limited 2018. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.ail.base;

import com.ail.core.BaseException;
import com.ail.core.CoreProxy;
import com.ail.core.RestfulServiceInvoker;
import com.ail.core.RestfulServiceReturn;
import com.ail.core.product.ProductServiceCommand;
import com.ail.core.product.ProductChangeEvenProcessorService.ProductChangeEvenProcessorCommand;
import com.ail.pageflow.ExecutePageActionService.ExecutePageActionArgument;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;
import static java.net.HttpURLConnection.HTTP_OK;

@ProductServiceCommand(serviceName = "ProcessChangeEventsService", commandName = "ProcessChangeEvents")
public class ProcessChangeEvents extends RestfulServiceInvoker {

	public static void invoke(ExecutePageActionArgument args) throws BaseException {
		new ProcessChangeEvents().invoke();
	}

	public RestfulServiceReturn service() throws BaseException {
		try {
			new CoreProxy().newCommand(ProductChangeEvenProcessorCommand.class).invoke();
			return new Return(HTTP_OK);
		} catch (Throwable t) {
			return new Return(HTTP_INTERNAL_ERROR, t.getMessage());
		}
	}

	public static class Return extends RestfulServiceReturn {
		String message;

		public Return(int status) {
			super(status);
		}

		public Return(int status, String message) {
			super(status);
			this.message = message;
		}
	}
}