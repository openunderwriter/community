/* Copyright Applied Industrial Logic Limited 2018. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.ail.base;

import static com.ail.core.persistence.hibernate.HibernateSessionBuilder.getSessionFactory;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;
import static java.net.HttpURLConnection.HTTP_OK;

import com.ail.core.BaseException;
import com.ail.core.Functions;
import com.ail.core.JSONException;
import com.ail.core.PostconditionException;
import com.ail.core.RestfulServiceInvoker;
import com.ail.core.RestfulServiceReturn;
import com.ail.core.key.ReservedKey;
import com.ail.core.product.ProductServiceCommand;
import com.ail.pageflow.ExecutePageActionService.ExecutePageActionArgument;

/**
 * Administration service to import reserved keys.
 */
@ProductServiceCommand(commandName = "ImportReservedKeys")
public class ImportReservedKeysService extends RestfulServiceInvoker {

	public static void invoke(ExecutePageActionArgument args) throws BaseException {
		new ImportReservedKeysService().invoke(Argument.class);
	}

	public RestfulServiceReturn service(Argument args) throws PostconditionException, JSONException {
		if (Functions.isEmpty(args.keyId)) {
			return new Return(HTTP_BAD_REQUEST, "keyId argument was not specified.");
		}
		
		if (args.tokens == null) {
			return new Return(HTTP_BAD_REQUEST, "tokens argument cannot be null.");
		}

		try {
			for(String token: args.tokens) {
				 getSessionFactory().getCurrentSession().save(new ReservedKey(args.keyId, token));
			}

			getSessionFactory().getCurrentSession().flush();
			
			return new Return(HTTP_OK, String.format("%d keys imported.", args.tokens.length));
		} catch (Throwable e) {
			return new Return(HTTP_INTERNAL_ERROR, e.getMessage());
		}
	}

	public static class Argument {
		String keyId;
		String[] tokens;
	}
	
	public static class Return extends RestfulServiceReturn {
		String message;

		public Return(int status, String message) {
			super(status);
			this.message = message;
		}
	}
}